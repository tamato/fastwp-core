<?php

namespace Tamato\FastWP\Core\Utils;

class Cleaner
{

    static function cleanHead()
    {
        // clean head section
        remove_action( 'wp_head', 'wp_resource_hints', 2);
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');
        add_filter('show_admin_bar', '__return_false');

        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'start_post_rel_link');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'adjacent_posts_rel_link');
    }

    public function __construct()
    {

    }
}