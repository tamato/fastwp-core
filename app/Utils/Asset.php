<?php

namespace Tamato\FastWP\Core\Utils;

class Asset
{

    static function version($asset) {
        $postfix = '';
        $foldername = 'assets';
        $location = get_template_directory() . "/$foldername/" . $asset;
        $webdir = get_template_directory_uri();
        if (file_exists($location)) {
            $postfix = '?v=' . filemtime($location);
        }
        return $webdir . '/' . $asset . $postfix;
    }

    public function __construct()
    {

    }
}