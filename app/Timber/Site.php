<?php

namespace Tamato\FastWP\Core\Timber;

use Tamato\FastWP\Core\Utils\Cleaner;
use Tamato\FastWP\Core\Utils\Asset;

class Site
{

    public function __construct()
    {
    }

    public function go()
    {

        Cleaner::cleanHead();

        add_filter('timber/twig', function($twig) {
            $twig->addFunction(new \Twig_Function('asset_version', function($asset) {
                return Asset::version($asset);
            }));

            $twig->addFunction(new \Twig_Function('get_meta', function($key = null) {
                $meta = [
                    'type' => 'website',
                    'title' => 'Default title',
                    'url' => 'url',
                    'description' => 'Description',
                    'image' => 'Image',
                    'imageWidth' => 'width',
                    'imageHeight' => 'height'
                ];
                if ($key) return $meta[$key];
                return $meta;
            }));

            return $twig;
        });
    }

}